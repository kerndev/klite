/******************************************************************************
* Copyright (c) 2015-2025 jiangxiaogang<kerndev@foxmail.com>
*
* This file is part of KLite distribution.
*
* KLite is free software, you can redistribute it and/or modify it under
* the MIT Licence.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
******************************************************************************/
#ifndef __INTERNAL_H
#define __INTERNAL_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

/* Compatible with armcc5 armcc6 gcc icc */
#if defined ( __GNUC__ ) || (__ARMCC_VERSION >= 6100100)
#define __weak __attribute__((weak))
#endif

struct tcb_list
{
	struct tcb_node *head;
	struct tcb_node *tail;
};

struct tcb_node
{
	struct tcb_node *prev;
	struct tcb_node *next;
	struct tcb      *tcb;
};

struct tcb
{
	void            *stack;
	void           (*entry)(void*);
	uint32_t         prio;
	uint32_t         time;
	uint32_t         timeout;
	struct tcb_list *list_sched;
	struct tcb_list *list_wait;
	struct tcb_node  node_sched;
	struct tcb_node  node_wait;
	uint32_t         stack_base[];
};

extern struct tcb *sched_tcb_now;
extern struct tcb *sched_tcb_next;

void        sched_init(void);
void        sched_tick(uint32_t time);
uint32_t    sched_idle(void);
void        sched_switch(void);
void        sched_preempt(void);
void        sched_reset(struct tcb *tcb, uint32_t prio);
void        sched_remove(struct tcb *tcb);
void        sched_ready(struct tcb *tcb);
void        sched_sleep(uint32_t timeout);
void        sched_timed_wait(struct tcb_list *list, uint32_t timeout);
void        sched_wait(struct tcb_list *list);
struct tcb *sched_wake_from(struct tcb_list *list);

void        cpu_sys_init(void);
void        cpu_sys_start(void);
void        cpu_sys_sleep(uint32_t time);
void        cpu_enter_critical(void);
void        cpu_leave_critical(void);
void        cpu_contex_switch(void);
void       *cpu_contex_init(uint32_t *stack_base, uint32_t stack_size, void *entry, void *arg);

#endif
