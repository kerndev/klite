/******************************************************************************
* Copyright (c) 2015-2025 jiangxiaogang<kerndev@foxmail.com>
*
* This file is part of KLite distribution.
*
* KLite is free software, you can redistribute it and/or modify it under
* the MIT Licence.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
******************************************************************************/
#include <stdio.h>
#include <string.h>
#include "kernel.h"
#include "intc.h"

typedef struct
{
	uint32_t TCFG0;
	uint32_t TCFG1;
	uint32_t TCON;
	uint32_t TCNTB0;
	uint32_t TCMPB0;
	uint32_t TCNTO0;
	uint32_t TCNTB1;
	uint32_t TCMPB1;
	uint32_t TCNTO1;
	uint32_t TCNTB2;
	uint32_t TCMPB2;
	uint32_t TCNTO2;
	uint32_t TCNTB3;
	uint32_t TCMPB3;
	uint32_t TCNTO3;
	uint32_t TCNTB4;
	uint32_t TCNTO4;
}TIMER_MMIO;

#define TIMER ((TIMER_MMIO*)0x51000000)

static void systick_handler(void)
{
	kernel_tick(10);
}

void cpu_sys_init(void)
{
	intc_init();
	intc_set_handler(IRQ_TIMER4, systick_handler);
	intc_enable_irq(IRQ_TIMER4);
}

void cpu_sys_start(void)
{
	TIMER->TCFG0 = 0xffff;
	TIMER->TCNTB4 = 950;
	TIMER->TCON = 7 << 20;
}

void cpu_sys_sleep(uint32_t time)
{

}
