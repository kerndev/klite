#ifndef __INTC_H
#define __INTC_H

typedef enum
{
	IRQ_EINT0 = 0,
	IRQ_EINT1,
	IRQ_EINT2,
	IRQ_EINT3,
	IRQ_EINT4_7,
	IRQ_EINT8_23,
	IRQ_RSVD1,
	IRQ_BATTFLT,
	IRQ_TICK,
	IRQ_WDT,
	IRQ_TIMER0,
	IRQ_TIMER1,
	IRQ_TIMER2,
	IRQ_TIMER3,
	IRQ_TIMER4,
	IRQ_UART2,
	IRQ_LCD,
	IRQ_DMA0,
	IRQ_DMA1,
	IRQ_DMA2,
	IRQ_DMA3,
	IRQ_SDI,
	IRQ_SPI0,
	IRQ_UART1,
	IRQ_RSVD2,
	IRQ_USBD,
	IRQ_USBH,
	IRQ_IIC,
	IRQ_UART0,
	IRQ_SPI1,
	IRQ_RTC,
	IRQ_ADC,
	IRQ_MAX,
}irq_type_t;

void intc_init(void);
void intc_set_handler(irq_type_t irq, void (*handler)(void));
void intc_set_priority(irq_type_t irq, int prio);
void intc_enable_irq(irq_type_t irq);
void intc_disable_irq(irq_type_t irq);

#endif
