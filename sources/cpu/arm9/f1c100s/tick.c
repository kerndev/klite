/******************************************************************************
* Copyright (c) 2015-2025 jiangxiaogang<kerndev@foxmail.com>
*
* This file is part of KLite distribution.
*
* KLite is free software, you can redistribute it and/or modify it under
* the MIT Licence.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
******************************************************************************/
#include "kernel.h"
#include "intc.h"
#include "f1c100s.h"

#define SYSTICK_IRQ IRQ_TIMER0

static void systick_handler(void)
{
	kernel_tick(1);
	TIMER->ISR |= 0x01;
}

static void systick_start(void)
{
	TIMER->TIM0_CTRL &= ~0x01;
	TIMER->ISR |= 0x01;
	TIMER->IER |= 0x01;
	TIMER->TIM0_INTV = 24000;
	TIMER->TIM0_CTRL = 0x4;
	TIMER->TIM0_CTRL |= 0x02;
	while(TIMER->TIM0_CTRL & 0x02);
	TIMER->TIM0_CTRL |= 0x01;
}

void cpu_sys_init(void)
{
	intc_init();
}

void cpu_sys_start(void)
{
	systick_start();
	intc_set_priority(SYSTICK_IRQ, 0); //0 is lowest
	intc_set_handler(SYSTICK_IRQ, systick_handler);
	intc_enable_irq(SYSTICK_IRQ);
}

void cpu_sys_sleep(uint32_t time)
{

}
