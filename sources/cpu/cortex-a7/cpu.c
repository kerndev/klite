/******************************************************************************
* Copyright (c) 2015-2025 jiangxiaogang<kerndev@foxmail.com>
*
* This file is part of KLite distribution.
*
* KLite is free software, you can redistribute it and/or modify it under
* the MIT Licence.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "kernel.h"
#include "intc.h"

static uint32_t m_lock_nesting;
static uint32_t m_cpsr_backup;

__asm uint32_t arm_disable_irq(void)
{
	MRS    R0, CPSR
	ORR    R1, R0, #0x80
	MSR    CPSR_c, R1
	BX     LR
}

__asm void arm_restore_irq(uint32_t cpsr)
{
	MSR    CPSR_c, R0
	BX     LR
}

void cpu_enter_critical(void)
{
	uint32_t cpsr = arm_disable_irq();
	if(m_lock_nesting++ == 0)
	{
		m_cpsr_backup = cpsr;
	}
}

void cpu_leave_critical(void)
{
	if(--m_lock_nesting == 0)
	{
		arm_restore_irq(m_cpsr_backup);
	}
}

void *cpu_contex_init(uint8_t *stack_base, uint32_t stack_size, void *entry, void *arg)
{
	uint32_t *sp = (uint32_t *)(stack_base + stack_size);
	memset(stack_base, 0xCC, stack_size);
	*(--sp) = 0x0000005F;               // CPSR:SYS,FIQ=disable,IRQ=enable
	*(--sp) = (uint32_t)entry;          // PC
	*(--sp) = (uint32_t)thread_exit;    // LR
	*(--sp) = 12;                       // R12
	*(--sp) = 11;                       // R11
	*(--sp) = 10;                       // R10
	*(--sp) = 9;                        // R9
	*(--sp) = 8;                        // R8
	*(--sp) = 7;                        // R7
	*(--sp) = 6;                        // R6
	*(--sp) = 5;                        // R5
	*(--sp) = 4;                        // R4
	*(--sp) = 3;                        // R3
	*(--sp) = 2;                        // R2
	*(--sp) = 1;                        // R1
	*(--sp) = (uint32_t)arg;            // R0
	return sp;
}

void cpu_contex_switch(void)
{
	intc_set_pending(0);
}

void cpu_sys_init(void)
{
	intc_init();
	intc_set_handler(0, NULL);
	intc_set_priority(0, 0);
	intc_enable_irq(0);
}

static void tick_handler(void)
{
	kernel_tick(1);
}

void cpu_sys_start(void)
{
	intc_set_handler(87, tick_handler);
	intc_enable_irq(87);
}

void cpu_sys_sleep(uint32_t time)
{

}
