;/******************************************************************************
;/* @file   interrupt.s
;/* @brief  interrupt for Cortex-A7
;/* @date   2025-01-19
;/* @author kerndev@foxmail.com
;/*****************************************************************************/
	AREA |.text|, CODE, READONLY
	PRESERVE8

;/*****************************************************************************/
; Mode bits and interrupt flag (I&F) defines
SYS_MODE         EQU    0x1F
SYS_MODE_NO_INT  EQU    0xDF

;/*****************************************************************************/
; IRQ Mode
IRQ_Handler PROC
	EXPORT IRQ_Handler
	IMPORT intc_handler
	IMPORT sched_tcb_now
	IMPORT sched_tcb_next
	SUB    LR, LR, #4
	SRSFD  SP!, #SYS_MODE
	MSR    CPSR_c, #SYS_MODE_NO_INT
	STMFD  SP!, {R0-R12, LR}
	BL     intc_handler
	LDR    R0, =sched_tcb_now
	LDR    R1, =sched_tcb_next
	LDR    R2, [R0]
	LDR    R3, [R1]
	CMP    R2, R3
	BEQ    RETURN
	STR    R3, [R0]
	CMP    R2, #0
	STRNE  SP, [R2]
	LDR    SP, [R3]
RETURN
	LDMFD  SP!, {R0-R12, LR}
	RFEFD  SP!
	ENDP

	END
