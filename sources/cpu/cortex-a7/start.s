;/******************************************************************************
;/* @file   start.s
;/* @brief  start code for Cortex-A7
;/* @date   2025-01-16
;/* @author kerndev@foxmail.com
;/*****************************************************************************/
Stack_Size     EQU     0x00000400
Heap_Size      EQU     0x00000000

	AREA STACK, NOINIT, READWRITE, ALIGN=3
Stack_Mem      SPACE   Stack_Size

	AREA HEAP, NOINIT, READWRITE, ALIGN=3
Heap_Mem       SPACE   Heap_Size

;/*****************************************************************************/
; User Initial Stack & Heap
	AREA |.text|, CODE, READONLY
	EXPORT __user_initial_stackheap
__user_initial_stackheap PROC
	LDR    R0, = Heap_Mem
	LDR    R1, = (Stack_Mem + Stack_Size)
	LDR    R2, = (Heap_Mem +  Heap_Size)
	LDR    R3, = Stack_Mem
	BX     LR
	ENDP

;/*****************************************************************************/
; Vector_Table
	AREA RESET, CODE, READONLY
	IMPORT IRQ_Handler
	EXPORT Reset_Handler
Vector_Table
	B      Reset_Handler
	B      Reset_Handler;Undefined_Addr
	B      Reset_Handler;SWI_Addr
	B      Reset_Handler;Prefetch_Addr
	B      Reset_Handler;Abort_Addr
	B      Reset_Handler;
	B      IRQ_Handler
	B      Reset_Handler;FIQ_Addr

;/*****************************************************************************/
; Mode bits and interrupt flag (I&F) defines
IRQ_MODE_NO_INT  EQU    0xD2
SVC_MODE_NO_INT  EQU    0xD3
SYS_MODE_NO_INT  EQU    0xDF

;/*****************************************************************************/
; Reset Handler
Reset_Handler PROC
	IMPORT __main
	MSR    CPSR_c, #SYS_MODE_NO_INT
	LDR    SP, =(Stack_Mem + Stack_Size)
	LDR    R0, =Vector_Table
	MCR    p15, 0, R0, c12, c0, 0
	LDR    R0, =__main
	BX     R0
	ENDP
	
	END
